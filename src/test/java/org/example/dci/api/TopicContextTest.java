package org.example.dci.api;

import org.example.dci.domain.Topic;
import org.example.dci.domain.traits.HasComments;
import org.example.dci.domain.traits.HasContent;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class TopicContextTest {

    @Test
    public void givenTopicContext_whenManipulating_thenExpectSuccess() {
        TopicContext context = new TopicContext();
        Topic topic = context.createTopic("Topic1.0", "blah1");
        assertThat(topic.getTitle().getText()).isEqualTo("Topic1.0");
        assertThat(topic.getContent().getText()).isEqualTo("blah1");
        topic = context.updateTopic(topic, "Topic1.1","blah1 blah1 blah1");
        assertThat(topic.getTitle().getText()).isEqualTo("Topic1.1");
        assertThat(topic.getContent().getText()).isEqualTo("blah1 blah1 blah1");
        HasComments firstComment = context.comment(topic, "comment 1.0");
        assertThat(firstComment.getContent().getText()).isEqualTo("comment 1.0");
        HasComments firstCommentNested = context.comment(firstComment, "comment 1.1 nested");
        assertThat(firstCommentNested.getContent().getText()).isEqualTo("comment 1.1 nested");
        HasComments secondComment = context.comment(topic, "comment 2.0");
        assertThat(secondComment.getContent().getText()).isEqualTo("comment 2.0");
        assertThat(topic.getComments().size()).isEqualTo(2);

        HasComments secondCommentNestedA = context.comment(secondComment, "comment 2.a nested");
        assertThat(secondCommentNestedA.getContent().getText()).isEqualTo("comment 2.a nested");
        HasComments secondCommentNestedB = context.comment(secondCommentNestedA, "comment 2.b nested");
        assertThat(secondCommentNestedB.getContent().getText()).isEqualTo("comment 2.b nested");
        HasComments secondCommentNestedC = context.comment(secondCommentNestedB, "comment 2.c nested");
        assertThat(secondCommentNestedC.getContent().getText()).isEqualTo("comment 2.c nested");

        Object object = topic;
        assertThat(Optional.of(object).map(o -> (HasComments<?> & HasContent<?>) o).filter(sum -> sum.getContent() != null && !sum.getComments().isEmpty())).isNotEmpty();

        assertThat(secondComment.getComments().size()).isEqualTo(1);
        assertThat(secondCommentNestedA.getComments().size()).isEqualTo(1);
        assertThat(secondCommentNestedB.getComments().size()).isEqualTo(1);
        assertThat(secondCommentNestedC.getComments()).isEmpty();
    }
}
