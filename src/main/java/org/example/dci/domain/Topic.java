package org.example.dci.domain;

import org.example.dci.domain.traits.HasAttachments;
import org.example.dci.domain.traits.HasComments;
import org.example.dci.domain.traits.HasContent;
import org.example.dci.domain.traits.HasTitle;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Topic implements HasTitle, HasContent, HasComments, HasAttachments {
    private UUID uuid;
    private Title title;
    private Content content;
    private List<Attachment> attachments;
    private List<Comment> comments;

    public Topic(String title, String content, List<Attachment> attachments) {
        this.uuid = UUID.randomUUID();
        this.title = new Title(title);
        this.content = new Content(content);
        this.attachments = attachments;
        this.comments = new ArrayList<>();
    }

    @Override
    public Title getTitle() {
        return title;
    }

    @Override
    public Content getContent() {
        return content;
    }

    @Override
    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public List<Attachment> getAttachments() {
        return attachments;
    }

}
