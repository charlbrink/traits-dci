package org.example.dci.domain;

import org.example.dci.domain.traits.HasComments;
import org.example.dci.domain.traits.HasContent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Comment implements HasContent, HasComments {
    private UUID uuid;
    private Content content;
    private List<Comment> comments;

    public Comment(String content) {
        this.uuid = UUID.randomUUID();
        this.content = new Content(content);
        this.comments = new ArrayList<>();
    }

    @Override
    public Content getContent() {
        return content;
    }

    @Override
    public List<Comment> getComments() {
        return comments;
    }

}
