package org.example.dci.domain;

import org.example.dci.domain.traits.HasBlob;
import org.example.dci.domain.traits.HasTitle;

import java.util.UUID;

public class Attachment implements HasTitle, HasBlob {
    private UUID uuid;
    private Title title;
    private byte[] blob;

    public Attachment(String title, byte[] blob) {
        this.uuid = UUID.randomUUID();
        this.title = new Title(title);
        this.blob = blob;
    }

    @Override
    public Title getTitle() {
        return title;
    }

    @Override
    public byte[] getBlob() {
        return blob;
    }

}
