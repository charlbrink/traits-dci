package org.example.dci.domain;

import java.util.UUID;

public class Title {
    private UUID uuid;

    private String text;

    public Title(String text) {
        this.uuid = UUID.randomUUID();
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
