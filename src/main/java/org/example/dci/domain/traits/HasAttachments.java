package org.example.dci.domain.traits;

import org.example.dci.domain.Attachment;

import java.util.List;

public interface HasAttachments<T extends HasAttachments<T>> {
    List<Attachment> getAttachments();

    default T addAttachment(Attachment attachment) {
        getAttachments().add(attachment);
        return (T) this;
    }

    default T deleteAttachment(Attachment attachment) {
        getAttachments().remove(attachment);
        return (T) this;
    }

}
