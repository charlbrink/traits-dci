package org.example.dci.domain.traits;

import org.example.dci.domain.Content;

public interface HasContent<T extends HasContent<T>> {
    Content getContent();

    default T setContent(String content) {
        getContent().setText(content);
        return (T) this;
    }

}

