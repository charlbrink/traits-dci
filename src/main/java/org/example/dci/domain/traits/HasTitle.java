package org.example.dci.domain.traits;

import org.example.dci.domain.Title;

public interface HasTitle<T extends HasContent<T>> {
    Title getTitle();

    default T setTitle(String title) {
        getTitle().setText(title);
        return (T) this;
    }

}
