package org.example.dci.domain.traits;

import org.example.dci.domain.Comment;
import org.example.dci.domain.Content;

import java.util.List;

public interface HasComments<T extends HasComments<T>> {
    Content getContent();
    List<HasComments> getComments();

    default T addComment(Comment comment) {
        getComments().add(comment);
        return (T) this;
    }

    default T deleteComment(Comment comment) {
        getComments().remove(comment);
        return (T) this;
    }

}
