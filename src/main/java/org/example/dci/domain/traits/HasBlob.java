package org.example.dci.domain.traits;

public interface HasBlob<T extends HasBlob<T>> {
    byte[] getBlob();
}
