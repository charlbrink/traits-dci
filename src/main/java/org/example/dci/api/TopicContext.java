package org.example.dci.api;

import org.example.dci.domain.Attachment;
import org.example.dci.domain.Comment;
import org.example.dci.domain.Topic;
import org.example.dci.domain.traits.HasComments;

import java.util.Arrays;

public class TopicContext {
    public Topic createTopic(String title, String content, Attachment... attachment) {
        return new Topic(title, content, Arrays.asList(attachment));
    }

    public Topic updateTopic(Topic topic, String title, String content, Attachment... attachment) {
        topic.setTitle(title);
        topic.setContent(content);
        topic.getAttachments().clear();
        topic.getAttachments().addAll(Arrays.asList(attachment));
        return topic;
    }

    public Topic addAttachments(Topic topic, Attachment... attachment) {
        topic.getAttachments().addAll(Arrays.asList(attachment));
        return topic;
    }

    public Topic removeAttachments(Topic topic, Attachment... attachment) {
        topic.getAttachments().removeAll(Arrays.asList(attachment));
        return topic;
    }

    public HasComments comment(HasComments hasComments, String content) {
        Comment comment = new Comment(content);
        hasComments.addComment(comment);
        return comment;
    }

}
